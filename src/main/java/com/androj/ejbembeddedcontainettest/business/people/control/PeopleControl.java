/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.androj.ejbembeddedcontainettest.business.people.control;

import com.androj.ejbembeddedcontainettest.business.people.entity.Person;
import java.util.List;
import javax.enterprise.context.RequestScoped;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.TypedQuery;

/**
 *
 * @author andrzejhankus
 */
@RequestScoped
public class PeopleControl {
    
    @PersistenceContext
    EntityManager em;
    
    public Person createPerson(Person toSave){
        return em.merge(toSave);
    }

    public Person find(Long id) {
        return em.find(Person.class, id);
    }

    public List<Person> findAll() {
        return em.createNamedQuery(Person.FIND_ALL, Person.class).getResultList();
    }
}
