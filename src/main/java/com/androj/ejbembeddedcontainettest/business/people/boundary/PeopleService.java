/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.androj.ejbembeddedcontainettest.business.people.boundary;

import com.androj.ejbembeddedcontainettest.business.external.control.IExternalSystemConnector;
import com.androj.ejbembeddedcontainettest.business.people.control.PeopleControl;
import com.androj.ejbembeddedcontainettest.business.people.entity.Person;
import java.util.List;
import javax.ejb.Stateless;
import javax.inject.Inject;

/**
 *
 * @author andrzejhankus
 */
@Stateless
public class PeopleService {
    
    @Inject
    private PeopleControl peopleControl;
    
    @Inject
    private IExternalSystemConnector external;
    
    public Person createPerson(String name, String surname){
        return peopleControl.createPerson(new Person(name, surname));
    }
    
    public Person find(Long id){
        return peopleControl.find(id);
    }

    public List<Person> findAll() {
        return peopleControl.findAll();
    }
    
    public String getResponseFromExternal(){
        return external.response();
    }
    
}
