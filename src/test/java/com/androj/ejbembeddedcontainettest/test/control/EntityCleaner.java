/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.androj.ejbembeddedcontainettest.test.control;

import java.util.List;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

@Stateless
public class EntityCleaner {
    
    @PersistenceContext
    private EntityManager em;
    
    public void clear(List<Class> entitiesToRemove){
        entitiesToRemove
                .forEach(this::seekAndDestroy);
    }
    
    private void seekAndDestroy(Class entity){
        this.seek(entity).forEach(this::destroy);
        
    }
    private List seek(Class entity){
        return em.createQuery("Select e from "+entity.getSimpleName()+" e", entity).getResultList();
    }
    
    private void destroy(Object instance){
        em.remove(instance);
    }
}
