/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.androj.ejbembeddedcontainettest.business.people.boundary;

import com.androj.ejbembeddedcontainettest.business.people.entity.Person;
import com.androj.ejbembeddedcontainettest.test.control.EntityCleaner;
import java.util.Arrays;
import java.util.List;
import javax.ejb.embeddable.EJBContainer;
import javax.naming.NamingException;
import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.CoreMatchers.notNullValue;
import org.junit.After;
import static org.junit.Assert.assertThat;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.CoreMatchers.notNullValue;
import static org.junit.Assert.assertThat;

public class PeopleServiceIT {
    
    private PeopleService cut;
    private EntityCleaner cleaner;
    private static EJBContainer container;
    private final String BEANS_NAMESPACE = "java:global/classes/";
    private final List<Class> usedEntities = Arrays.asList(Person.class);
    
    @BeforeClass
    public static void initClass(){
        container = EJBContainer.createEJBContainer();
    }
    
    @Before
    public void init() throws NamingException{
        cut = (PeopleService)container.getContext().lookup(BEANS_NAMESPACE+"PeopleService");
        cleaner = (EntityCleaner)container.getContext().lookup(BEANS_NAMESPACE+"EntityCleaner");
    }
    
    @After
    public void clear(){
        cleaner.clear(usedEntities);
    }

    @Test
    public void beanTest() {
        assertThat(cut, is(notNullValue()));
    }
    
    @Test
    public void shouldReturnCreatedPerson(){
        String name = "Andrzej";
        String surname = "Hankus";
        Person createdPerson = this.createNewPerson(name, surname);
        assertThat(createdPerson.getName(),is(name) );
        assertThat(createdPerson.getSurname(),is(surname) );
    }
    
    @Test
    public void shouldCreateAndFindPerson(){
        String name = "Tony";
        String surname = "Stark";
        Long persistedPersonId = this.createNewPerson(name, surname).getId();
        
        Person foundPerson = cut.find(persistedPersonId);
        assertThat(foundPerson, is(notNullValue()));
        assertThat(foundPerson.getName(),is(name) );
        assertThat(foundPerson.getSurname(),is(surname) );
        
    }
    
    @Test
    public void shouldReturnTwoPeople(){
        this.createNewPerson("p1_name", "p1_surname");
        this.createNewPerson("p2_name", "p2_surname");
        
        
        List<Person> foundPeople = cut.findAll();
        
        assertThat(foundPeople, is(notNullValue()));
        assertThat(foundPeople.size(), is(2));

    }
    
    @Test
    public void shouldReceiveResponseFromAlternative(){
        assertThat(cut.getResponseFromExternal(), is("Response from alternative"));
    }
    
    private Person createNewPerson(String name,String surname){
        return cut.createPerson(name, surname);
    }
    
}
