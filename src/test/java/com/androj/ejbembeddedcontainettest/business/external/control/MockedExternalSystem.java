/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.androj.ejbembeddedcontainettest.business.external.control;

import javax.enterprise.context.RequestScoped;
import javax.enterprise.inject.Alternative;

@Alternative
@RequestScoped
public class MockedExternalSystem implements IExternalSystemConnector{

    @Override
    public String response() {
        return "Response from alternative";
    }
    
}
